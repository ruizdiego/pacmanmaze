﻿using System.Collections;
using System.Collections.Generic;

// Unity Framework
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapManagerU : MonoBehaviour
{
    public Tilemap level;
    public Tilemap markers;

    public GameObject playerPrefab;
    
    public int minCol = 0;
    public int maxCol = 1;
    public int minRow = 0;
    public int maxRow = 1;

    private Grid _grid;
    
    // Start is called before the first frame update
    void Start()
    {
        _grid = GetComponent<Grid>();
        
        markers.gameObject.SetActive(false);
        
        PlacePlayer();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void PlacePlayer()
    {
        Vector3Int mapPos;

        for (int col = minCol; col <= maxCol; col++)
        {
            for (int row = minRow; row <= maxRow; row++)
            {
                mapPos = new Vector3Int(col, row, 0);
                
                var spr = markers.GetSprite(mapPos);
                if (spr != null)
                {
                    var ofs = new Vector3(markers.tileAnchor.x * _grid.cellSize.x, markers.tileAnchor.y * _grid.cellSize.y, 0f);
                    var worldPos = markers.CellToWorld(mapPos) + ofs;
                    Debug.Log($"spr: {spr.name} col: {col} row: {row} worldPos: {worldPos} {ofs}");

                    var player = GameObject.Instantiate(playerPrefab, worldPos, Quaternion.identity);
                }
            }
        }
    }
}