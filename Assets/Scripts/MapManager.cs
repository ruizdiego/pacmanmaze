﻿// Mono Framework
using System.Collections.Generic;
using System.IO;

// Unity Framework
using UnityEngine;

public class MapManager : MonoBehaviorSingleton<MapManager> 
{
    public Sprite[] mapTiles;

    public char tile0Char = 'a';
	public char dotChar = 'J';
	public char playerStartChar = '1';

    public char defChar = ' ';

    public int cols = 24;
    public int rows = 31;

    public int tileWidth = 16;
    public int tileHeight = 16;

    private int[,] _mapData;

    private  TileInfo[,] _mapSprites;

    private int _ofsx;
    private int _ofsy;

	/// <summary>
	/// Unity Start Method
	/// </summary>
	void Start()
	{
		_ofsx = ((cols * tileWidth) / 2) - (tileWidth / 2);
		_ofsy = (rows * tileHeight) / 2 - (tileHeight / 2);

		LoadLevel(1);
		CreateMap();
	}
    void LoadLevel(int lvl)
    {
        TextAsset ta = Resources.Load<TextAsset>(string.Format("level{0}", lvl));
        StringReader sr = new StringReader(ta.text);

        _mapData = new int[rows, cols];

        for (int r = 0; r < rows; r++)
        {
            string line = sr.ReadLine();

            if (string.IsNullOrEmpty(line))
            {
				// El mapa especifica más filas de las que tiene el archivo, se
				// completa con defChar
				for (int c = 0; c < cols; c++)
				{
					_mapData[r, c] = defChar;
				}
            }
            else
            {
                for (int c = 0; c < cols; c++)
                {
                    if (c < line.Length)
                    {
						if (line[c] == playerStartChar)
						{
							// Dejo limpio en la estructura la celda donde ubico al jugador
							_mapData[r, c] = defChar;
						}
						else
							_mapData[r, c] = line[c];

                    }
                    else
                    {
                        // La línea leída no tiene tantos caracteres como
                        // especifica la estructura del mapa que debe tener,
                        // se completa con defChar
                        _mapData[r, c] = defChar;
                    }
                }
            }
        }
    }

    void CreateMap()
    {
        if (_mapData != null)
        {
            if (_mapSprites == null) _mapSprites = new TileInfo[rows, cols];

            GameObject tile;
            SpriteRenderer sprRnd;
            TileInfo ti;

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
					if (_mapData[r, c] != defChar && _mapData[r, c] != playerStartChar)
                    {
                        if (_mapSprites[r, c] == null)
                        {
                            tile = new GameObject(string.Format("tile {0} - {1}", r, c)); 
                            sprRnd = tile.AddComponent<SpriteRenderer>();
                            ti = tile.AddComponent<TileInfo>();
                            _mapSprites[r, c] = ti;
                        }
                        else
                        {
                            tile = _mapSprites[r, c].gameObject;
                            sprRnd = tile.GetComponent<SpriteRenderer>();
                            ti = tile.GetComponent<TileInfo>();
                        }

						sprRnd.sprite = mapTiles[_mapData[r, c] - tile0Char];
						tile.transform.position = new Vector3(c * tileWidth - _ofsx, _ofsy - r * tileHeight, 0f);
						tile.transform.SetParent(gameObject.transform);

						ti.tileNum = _mapData[r, c];
						ti.sprRnd = sprRnd;
                    }
                }
            }
        }
    }

	/// <summary>
	/// Retorna la posición de mundo de la celda específicada
	/// (el cero local de la celda es su centro)
	/// <returns></returns>
	public Vector3 GetWorldPos(int row, int col)
	{
		return new Vector3(col * tileWidth - _ofsx, _ofsy - row * tileHeight, 0f);
	}

	/// <summary>
	/// Retorna verdero si la posición especificada se encuentra dentro del mapa
	/// y el row/col de dicha posición en los parámetros de salida
	/// <returns></returns>
	public bool GetRowCol(Vector3 pos, out int row, out int col)
	{
		row = (int) Mathf.Floor((pos.y - _ofsy - (tileHeight / 2)) / -tileHeight);
		col = (int) Mathf.Floor((pos.x + _ofsx + (tileWidth / 2)) / tileWidth);
		return (0 <= row && row < rows && 0 <= col && col < cols);
	}

	/// <summary>
	/// Retorna verdadero si la celda se encuentra vacía o con un dot
	/// </summary>
	public bool IsWalkable(int row, int col)
	{
		return (_mapData[row, col] == defChar || _mapData[row, col] == dotChar);
	}

	/// <summary>
	/// Retorna verdadero si la celda se encuentra vacía o con un dot
	/// </summary>
	public bool IsWalkable(Vector3 pos)
	{
		int r, c;
		if (GetRowCol(pos, out r, out c))
			return IsWalkable(r, c);
		else
			return false;
	}

	public Vector3 ClampWorldPos(Vector3 pos)
	{
		int r, c;
		if (GetRowCol(pos, out r, out c))
		{
			return GetWorldPos(r, c);
		}
		else
			return Vector3.zero;
	}

	/// <summary>
	/// Remueve el dot de la posición específicada
	/// </summary>
	public bool EatDot(int row, int col)
	{
		if (_mapData[row, col] == dotChar)
		{
			_mapData[row, col] = defChar;
			_mapSprites[row, col].sprRnd.sprite = null;
			return true;
		}
		else
			return false;
	}

}
